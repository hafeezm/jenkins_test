package com.openlbs.configuration.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.openlbs.configuration.OpenLBSConfiguration;

/**
 * The class GatewayServers hold the individual {@link Gateway} configuration.
 * 
 * @author Hafeez
 * @author Rangalal
 *
 */
@XmlRootElement(name = "GatewayServers")
public class GatewayServers extends OpenLBSConfiguration {

	private List<Gateway> gateways;

	/**
	 * @return the gateways
	 */
	public List<Gateway> getGateways() {
		return gateways;
	}

	/**
	 * @param gateways
	 *            the gateways to set
	 */
	@XmlElement(name = "Gateway")
	public void setGateways(List<Gateway> gateways) {
		this.gateways = gateways;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GatewayServers [gateways=" + gateways + "]";
	}

}
