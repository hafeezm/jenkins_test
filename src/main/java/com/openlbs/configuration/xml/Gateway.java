package com.openlbs.configuration.xml;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * The class Gateway hold the single gateway configuration values.
 * 
 * @author Hafeez
 * @author Rangalal
 * 
 *
 */
public class Gateway {

	/* the handlerClass is the implementation of the gateway */
	private String handlerClass;

	/* the gateway Socket port */
	private int port;

	private int queueSize;

	/* time out of a client connection in seconds */
	private int timeOutValue;

	/* Client connection limit (backlog) */
	private int clientLimit = 50;

	private int processingLimit;

	private String id;
	
	public Gateway() {
		super();
	}

	public Gateway(String handlerClass, int port, int queueSize, int timeOutValue, int clientLimit, int processingLimit,
			String id) {
		super();
		this.handlerClass = handlerClass;
		this.port = port;
		this.queueSize = queueSize;
		this.timeOutValue = timeOutValue;
		this.clientLimit = clientLimit;
		this.processingLimit = processingLimit;
		this.id = id;
	}

	/**
	 * @return the handlerClass
	 */
	public String getHandlerClass() {
		return handlerClass;
	}

	/**
	 * @param handlerClass
	 *            the handlerClass to set
	 */
	@XmlElement(name = "HandlerClass", required = true)
	public void setHandlerClass(String handlerClass) {
		this.handlerClass = handlerClass;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	@XmlElement(name = "Port", required = true)
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the queueSize
	 */
	public int getQueueSize() {
		return queueSize;
	}

	/**
	 * @param queueSize
	 *            the queueSize to set
	 */
	@XmlElement(name = "QueueSize")
	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}

	/**
	 * @return the timeOutValue
	 */
	public int getTimeOutValue() {
		return timeOutValue;
	}

	/**
	 * @param timeOutValue
	 *            the timeOutValue to set
	 */
	@XmlElement(name = "TimeOutValue")
	public void setTimeOutValue(int timeOutValue) {
		this.timeOutValue = timeOutValue;
	}

	/**
	 * @return the clientLimit
	 */
	public int getClientLimit() {
		return clientLimit;
	}

	/**
	 * @param clientLimit
	 *            the clientLimit to set
	 */
	@XmlElement(name = "ClientLimit", defaultValue = "50", required = false)
	public void setClientLimit(int clientLimit) {
		this.clientLimit = clientLimit;
	}

	/**
	 * @return the processingLimit
	 */
	public int getProcessingLimit() {
		return processingLimit;
	}

	/**
	 * @param processingLimit
	 *            the processingLimit to set
	 */
	@XmlElement(name = "ProcessingLimit")
	public void setProcessingLimit(int processingLimit) {
		this.processingLimit = processingLimit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@XmlAttribute(name = "id", required = true)
	public void setId(String id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Gateway [handlerClass=" + handlerClass + ", port=" + port + ", queueSize=" + queueSize
				+ ", timeOutValue=" + timeOutValue + ", clientLimit=" + clientLimit + ", processingLimit="
				+ processingLimit + ", id=" + id + "]";
	}

}
