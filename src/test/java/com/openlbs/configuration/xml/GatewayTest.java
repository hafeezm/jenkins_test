package com.openlbs.configuration.xml;

import org.junit.Assert;
import org.junit.Test;

public class GatewayTest {
	
	@Test
	public void gatewayDefaultValueTestSuccess() {
		
		Gateway gateway = new Gateway();
		
		Assert.assertEquals("client limit is not matched", 50, gateway.getClientLimit());
		
	}

}
